// console.log("Hello World");

// function showMessage(message1){
// 	console.log("You are nothing, so why try?");
// };

// showMessage();
// showMessage();
// showMessage();
// showMessage();
// showMessage();

//While loop
// A while loop allows us to repeat a task/code while the condition is true.

/*
	while(condition){
	

		task;
		increment/decrement;
	}
*/

// variable for counter, it will set how many times we will be able to repeat our task.

// let count = 5;

// As long as our count variable's value is not equal to zero, we will be able to repeat our task

// while(count !== 0){

// 	showMessage();
// 	// If a loop is not able to falsify its condition, you will be facing an infinite loop.
// 	count--;
// }

// while (count !== 0){

// 	console.log(count);
// 	count--;
// }

// First time loop run: count = 5 -> before we start the next loop, we decrement to 4;
// Second time loop run: count = 4 -> before we start the next loop, we decrement to 3;
// Third time loop run: count = 3 -> before we start the next loop, we decrement to 2;
// Fourth time loop run: count = 2 -> before we start the next loop, we decrement to 1;
// Fifth time loop run: count = 1 -> before we start the next loop, we decrement to 0;

// On a sixth "possible" loop, is our condition still true?

// Do While loops
// Do While loops are similar to while loops that it allows us to repeat actions/tasks as long as the condition is true. However, with a do-while loop, you are able to perform a task at least once even if the condition is not true.

// do {
// 	console.log(count);
// 	count--;
// } while (count === 0);

// while(count === 0){
// 	console.log(count);
// 	count--;
// }

// let counterMini = 20;

// while (counterMini > 0){
// 	console.log(counterMini);
// 	counterMini--;
// }

// For Loop
// The for loop is a more flexible version of our while and do while loops.
// It consists of 3 parts.
// 1. the declaration/initialization of the counter.
// 2. the condition that will be evaluated to determine if the loop will continue
// 3. the iteration or the incrementation/decrementation need to continue and arrive at a terminating/end condition

// for(let count = 0; count <= 20; count++){
// 	console.log(count);
// }

// First loop = count = 0 = before we end the loop -> count = 1
// Second Loop = 1 = before we emd the loop -> count = 2
// 2nd to the Last Loop = count = 19 = before we end the loop -> count = 20
// Last Loop = count = 20 = before we end the last loop -> count = 21
// Will we run another loop? No. Because count no longer meets our condition.

// for (let x = 1; x < 10; x++){
// 	let sum = 1 + x
// 	console.log("The sum of " + 1 + " + " + x + " = " + sum);
// }

// Continue and Break
/*

	Continue is a keyword that allows the code to go to the next loop without finishing the current code block

*/

// for(let counter = 0; counter <= 20; counter++){
	
// 	// When counter === 11

// 	// If the current value of count is even
// 	if(counter % 2 === 0){
// 		continue;
// 	}
// 	// When we use the continue keyword, the code block following is disregarded and the next loop is run.
// 	// Whenever the value of counter is an even number, we skip to the next loop.
// 	// Therefore, only odd numbers are shown.

// 	console.log(counter);

// 	/*
// 		Break - allows us to end the execution of a loop.
// 	*/
// 	if(counter > 10){
// 		break;
// 	}
// }

for(let counterMini = 0; counterMini <= 100; counterMini++){
	if(counterMini % 5 === 0){
		console.log(counterMini);
	}
}
